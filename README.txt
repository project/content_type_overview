Content type overview
================================================================================

Summary
--------------------------------------------------------------------------------

Provides easy access to all basic content type settings.


Requirements
--------------------------------------------------------------------------------

No requirements.


Installation
--------------------------------------------------------------------------------

1. Copy the content_type_overview folder to sites/all/modules or to a
   site-specific modules folder.

2. Go to Administer > Site building > Modules and enable the Content type
   overview module.


Configuration
--------------------------------------------------------------------------------

1. Go to Administer > Site configuration > Content type overview and select the
   content types you want to display on the over view page.

You can now go to Administer > Content management > Content types and click the
new Overview tab to edit all selected content type settings in one place.


Supported modules
--------------------------------------------------------------------------------

Currently, the module handles form elements supplied by the following core and
contributed modules:

 * Comments (core)
 * Automatic Nodetitles (contrib)


Support
--------------------------------------------------------------------------------

Please post bug reports and feature requests in the issue queue:

  http://drupal.org/project/content_type_overview


Credits
--------------------------------------------------------------------------------

Author: Morten Wulff <wulff@ratatosk.net>


